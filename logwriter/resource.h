//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ で生成されたインクルード ファイル。
// logwriter.rc で使用
//
#define IDD_LOGWRITER_DIALOG            102
#define IDR_MAINFRAME                   128
#define IDS_DIALOG_TITLE_ERROR          130
#define IDS_MSG_ERROR_REQUIRED_EVENTSOURCENAME 131
#define IDS_MSG_ERROR_CANNOTOPEN_EVENTLOG 132
#define IDS_MSG_ERROR_CANNOTGET_CREDENTIAL 133
#define IDS_MSG_ERROR_CANNOTWRITE_EVENTLOG 134
#define IDS_MSG_SUCCEED_LOGGING         135
#define IDS_DIALOG_TITLE_INFO           136
#define IDC_EVENTID                     1001
#define IDC_LOGMESSAGE                  1002
#define IDC_LOGLEVEL                    1003
#define IDC_DO_WRITE                    1004
#define IDC_DO_CLEAR                    1005
#define IDC_LOGTYPE                     1006
#define IDC_TASKCATEGORY                1007

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1005
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
