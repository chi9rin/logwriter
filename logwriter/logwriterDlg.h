#pragma once
#include "afxwin.h"


class CLogWriterDlg : public CDialogEx
{
public:
	enum { IDD = IDD_LOGWRITER_DIALOG };
	CLogWriterDlg(CWnd* pParent = NULL);
	void ClearControls();
	afx_msg void OnBnClickedDoClear();
	afx_msg void OnBnClickedDoWrite();


protected:
	HICON m_hIcon;

	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	virtual void DoDataExchange(CDataExchange* pDX);
	DECLARE_MESSAGE_MAP()


private:
	CComboBox m_logType_ctl;
	int m_logType;
	CComboBox m_logLevel_ctl;
	int m_logLevel;
	CEdit m_eventID_ctl;
	int m_eventID;
	CEdit m_logMessage_ctl;
	CString m_logMessage;
	int m_taskCategory;
	CEdit m_taskCategory_ctl;

	void ShowMessageBox(UINT titleID, UINT msgID, UINT type);
	BOOL WriteLog();

};
