#include "stdafx.h"
#include <iostream>
#include "logwriter.h"
#include "logwriterDlg.h"
#include "afxdialogex.h"
#include "resource.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


CLogWriterDlg::CLogWriterDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CLogWriterDlg::IDD, pParent)
	, m_eventID(0)
	, m_logMessage(_T(""))
	, m_logType(0)
	, m_logLevel(0)
	, m_taskCategory(0)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

}


void CLogWriterDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LOGTYPE, m_logType_ctl);
	DDX_Control(pDX, IDC_LOGLEVEL, m_logLevel_ctl);
	DDX_Control(pDX, IDC_EVENTID, m_eventID_ctl);
	DDX_Text(pDX, IDC_EVENTID, m_eventID);
	DDV_MinMaxInt(pDX, m_eventID, 0, 65535);
	DDX_Control(pDX, IDC_LOGMESSAGE, m_logMessage_ctl);
	DDX_Text(pDX, IDC_LOGMESSAGE, m_logMessage);
	DDV_MaxChars(pDX, m_logMessage, 32767);
	DDX_CBIndex(pDX, IDC_LOGTYPE, m_logType);
	DDV_MinMaxInt(pDX, m_logType, 0, 1);
	DDX_CBIndex(pDX, IDC_LOGLEVEL, m_logLevel);
	DDV_MinMaxInt(pDX, m_logLevel, 0, 2);
	DDX_Control(pDX, IDC_TASKCATEGORY, m_taskCategory_ctl);
	DDX_Text(pDX, IDC_TASKCATEGORY, m_taskCategory);
	DDV_MinMaxInt(pDX, m_taskCategory, 0, 65535);

}


BEGIN_MESSAGE_MAP(CLogWriterDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_DO_CLEAR, &CLogWriterDlg::OnBnClickedDoClear)
	ON_BN_CLICKED(IDC_DO_WRITE, &CLogWriterDlg::OnBnClickedDoWrite)
END_MESSAGE_MAP()


BOOL CLogWriterDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	SetIcon(m_hIcon, TRUE);
	SetIcon(m_hIcon, FALSE);

	ClearControls();

	return TRUE;

}


void CLogWriterDlg::OnPaint()
{
	if (IsIconic())	{
		CPaintDC dc(this);

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		dc.DrawIcon(x, y, m_hIcon);

	} else {
		CDialogEx::OnPaint();

	}
}


HCURSOR CLogWriterDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);

}


void CLogWriterDlg::ClearControls()
{
	m_logType_ctl.SetCurSel(0);
	m_logLevel_ctl.SetCurSel(0);
	m_eventID_ctl.Clear();
	m_taskCategory_ctl.Clear();
	m_logMessage_ctl.Clear();

	UpdateData(FALSE);

}


void CLogWriterDlg::OnBnClickedDoClear()
{
	ClearControls();

}


void CLogWriterDlg::OnBnClickedDoWrite()
{
	UpdateData();

	if (TRUE == WriteLog()) {
		ShowMessageBox(IDS_DIALOG_TITLE_INFO, IDS_MSG_SUCCEED_LOGGING, MB_ICONINFORMATION | MB_OK);

	} else {
		ShowMessageBox(IDS_DIALOG_TITLE_ERROR, IDS_MSG_ERROR_CANNOTWRITE_EVENTLOG, MB_ICONERROR | MB_OK);

	}

	return;

}


void CLogWriterDlg::ShowMessageBox(UINT titleID, UINT msgID, UINT type)
{
	CString dialogTitle, dialogMessage;

	dialogTitle.LoadString(titleID);
	dialogMessage.LoadString(msgID);

	MessageBox(dialogMessage, dialogTitle, type);

}


BOOL CLogWriterDlg::WriteLog()
{
	bool retval = TRUE;
	CString logSource[] = { _T("Application"), _T("System") };
	WORD logLevel[] = { EVENTLOG_INFORMATION_TYPE, EVENTLOG_WARNING_TYPE, EVENTLOG_ERROR_TYPE };
	LPCTSTR logMessage = static_cast<LPCTSTR>(m_logMessage);
	HANDLE hEventLogger = ::RegisterEventSource(NULL, logSource[m_logType]);

	if (NULL == hEventLogger) {
		ShowMessageBox(IDS_DIALOG_TITLE_ERROR, IDS_MSG_ERROR_CANNOTOPEN_EVENTLOG, MB_ICONSTOP | MB_OK);
		retval = FALSE;
		goto WRITELOG_OUT;

	}

	if (0 == ::ReportEvent(hEventLogger, logLevel[m_logLevel], m_taskCategory, m_eventID, NULL, 1, 0, &logMessage, NULL)) {
		retval = FALSE;
		goto WRITELOG_OUT;

	}

	::DeregisterEventSource(hEventLogger);

WRITELOG_OUT:
	return retval;

}
